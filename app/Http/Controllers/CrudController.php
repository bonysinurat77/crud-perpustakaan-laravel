<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{
    public function input(Request $request)
    {
        DB::table('rak_buku')->insert([
            'buku_judul' => $request->buku_judul,
            'buku_genre' => $request->buku_genre,
            'buku_penerbit' => $request->buku_penerbit,
            'buku_penulis' => $request->buku_penulis
        ]);
        return redirect('/bookshelf');
    }

    public function update(Request $request)
    {
        DB::table('rak_buku')->where('buku_id', $request->buku_id)->update([
            'buku_judul' => $request->buku_judul,
            'buku_genre' => $request->buku_genre,
            'buku_penerbit' => $request->buku_penerbit,
            'buku_penulis' => $request->buku_penulis
        ]);
        return redirect('/bookshelf');
    }

    public function delete($id)
    {
        DB::table('rak_buku')->where('buku_id', $id)->delete();
        return redirect('/bookshelf');
        
    }
}
