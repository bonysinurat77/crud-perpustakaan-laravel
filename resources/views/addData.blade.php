<!DOCTYPE html>
<html>
<head>
	<title>Add Data</title>
</head>
<body>
	<h3>Tambah Daftar Buku</h3>

	<a href="/bookshelf"> Go Back</a>
	
	<br/>
	<br/>

	<form action="/bookshelf/input" method="post">
		{{ csrf_field() }}
        Judul <input type="text" name="buku_judul" required="required"> <br/>
        Genre <input type="text" name="buku_genre" required="required"> <br/>
        Penerbit<input type="text" name="buku_penerbit" required="required"> <br/>
        Penulis<input type="text" name="buku_penulis" required="required"> <br/>
		<input type="submit" value="Input">
	</form>

</body>
</html>