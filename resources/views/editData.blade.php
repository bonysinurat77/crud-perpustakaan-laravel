<!DOCTYPE html>
<html>

<head>
    <title>Add Data</title>
</head>

<body>
    <h3>Edit Daftar Buku</h3>

    <a href="/bookshelf"> Go Back</a>

    <br />
    <br />

    @foreach($rak_buku as $rb)
    <form action="/bookshelf/update" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="buku_id" value="{{ $rb->buku_id }}"> <br/>
        Judul <input type="text" name="buku_judul" required="required" value="{{ $rb->buku_judul }}"> <br/>
        Genre <input type="text" name="buku_genre" required="required" value="{{ $rb->buku_genre }}"> <br/>
        Penerbit<input type="text" name="buku_penerbit" required="required" value="{{ $rb->buku_penerbit }}"> <br/>
        Penulis<input type="text" name="buku_penulis" required="required" value="{{ $rb->buku_penulis }}"> <br/>
        <input type="submit" value="Input">
    </form>
    @endforeach

</body>

</html>