<!DOCTYPE html>
<html>

<head>
	<title>CrudTest</title>
</head>

<body>
	<h3>Rak Buku</h3>

	<a href="/bookshelf/add"> + Tambahkan Buku</a>

	<br />
	<br />

	<table border="1">
		<tr>
			<th>Id</th>
			<th>Judul Buku</th>
			<th>Genre</th>
			<th>Penerbit</th>
			<th>Penulis</th>
		</tr>
		@foreach($rak_buku as $rb)
		<tr>
			<td scope="row"> {{ $loop->iteration }} </td>
			<td>{{ $rb->buku_judul }}</td>
			<td>{{ $rb->buku_genre }}</td>
			<td>{{ $rb->buku_penerbit }}</td>
			<td>{{ $rb->buku_penulis }}</td>
			<td><a href="/bookshelf/edit/{{ $rb->buku_id }}">Edit</a></td>
			<td><a href="/bookshelf/hapus/{{ $rb->buku_id }}">Delete</a></td>
		</tr>
		@endforeach
	</table>

	<br />

</body>

</html>